import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import DashboardIcon from '@material-ui/icons/Dashboard'
import { AddCircle, People } from '@material-ui/icons'
import { Link as RouterLink } from 'react-router-dom'

export const mainListItems = (
  <div>
    <ListItem button to="/" component={RouterLink}>
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItem>

    <ListItem button to="/users" component={RouterLink}>
      <ListItemIcon>
        <People />
      </ListItemIcon>
      <ListItemText primary="Users" />
    </ListItem>
    <ListItem button to="/users/create" component={RouterLink}>
      <ListItemIcon>
        <AddCircle />
      </ListItemIcon>
      <ListItemText primary="Create New User" />
    </ListItem>
  </div>
)
