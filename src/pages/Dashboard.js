import React from 'react'
import UsersList from '../components/User/UsersList'

export default function Dashboard() {
  return <UsersList recentLength={5} title="Recent users" />
}
