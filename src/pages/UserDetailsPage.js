import React from 'react'
import UserDetails from '../components/User/UserDetails'

export default function UserDetailsPage({ match }) {
  const id = match && match.params.id
  return <UserDetails id={id} />
}
