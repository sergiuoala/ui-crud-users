import React from 'react'
import { connect } from 'react-redux'
import get from 'lodash.get'

import Title from '../components/Title'
import { createUserAction, editUserAction } from '../components/User/userActions'
import UserForm from '../components/User/UserForm'

export function CreateUserPage({ match, users, createUser, editUser }) {
  const userId = get(match, 'params.id')
  const user = users.find(u => u.id === userId)
  const onFormSubmit = user ? editUser : createUser

  return (
    <div>
      <Title>{ user ? 'Edit User' : 'Create User'}</Title>
      <UserForm initialState={user} onFormSubmit={onFormSubmit} />
    </div>
  )
}

const mapStateToProps = ({ users }) => ({
  users,
})

const mapDispatchToProps = (dispatch) => ({
  createUser: (user) => dispatch(createUserAction(user)),
  editUser: (user) => dispatch(editUserAction(user)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateUserPage)
