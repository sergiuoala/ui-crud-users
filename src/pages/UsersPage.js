import React from 'react'
import UsersList from '../components/User/UsersList'

export default function UsersPage() {
    return (
        <UsersList />
    )
}
