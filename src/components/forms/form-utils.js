import get from 'lodash.get'

export const isValidField = (errors = {}, field = '') => get(errors, field, '').length > 0

export const isTouchedField = (touched = {}, field = '') => get(touched, field, false)