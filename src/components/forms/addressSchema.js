import * as Yup from 'yup'

export default Yup.object().shape({
  address: Yup.object().shape({
    city: Yup.string()
      .min(3, 'City name too short.')
      .max(50, 'City name too long')
      .required('City is a required field'),
    zipcode: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Zipcode is a required field.'),
    street: Yup.string()
      .min(3, 'Street name too short')
      .max(50, 'Street name too long')
      .required('Street name is a required field'),
    houseNumber: Yup.string()
      .min(1)
      .max(10)
      .required('House number is a required field.'),
  }),
})
