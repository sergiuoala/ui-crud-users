import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Layout from './Layout'
import Dashboard from '../pages/Dashboard'
import CreateUserPage from '../pages/CreateUserPage'
import UsersPage from '../pages/UsersPage'
import UserDetailsPage from '../pages/UserDetailsPage'
import { Provider } from 'react-redux'

export default function Root({ store }) {
  return (
    <Provider store={store}>
      <Router>
        <Layout>
          <Route exact path="/" component={Dashboard} />
          <Switch>
            <Route exact path="/users" component={UsersPage} />
            <Route exact path="/users/create" component={CreateUserPage} />
            <Route exact path="/users/:id" component={UserDetailsPage} />
            <Route exact path="/users/edit/:id" component={CreateUserPage} />
          </Switch>
        </Layout>
      </Router>
    </Provider>
  )
}
