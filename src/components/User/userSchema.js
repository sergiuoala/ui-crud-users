import * as Yup from 'yup';

export default Yup.object().shape({
    name: Yup.string()
        .min(3, 'Name is too short.')
        .max(50, 'Name is too long.')
        .required('Name is a required field.'),
    email: Yup.string()
        .email()
        .required('Email is a required field.'),
    phone: Yup.string().required('Phone number is required'),
    gender: Yup.string()
        .matches(/(male|female)/)
        .required('Gender is a required field.'),
});
