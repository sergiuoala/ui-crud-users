export const createUserAction = (user) => (dispatch) => {
  dispatch({
    type: 'USER_CREATE',
    payload: user,
  })
}

export const deleteUserAction = (userId = '') => (dispatch) => {
  dispatch({
    type: 'USER_DELETE',
    payload: {
      userId,
    },
  })
}

export const editUserAction = (user) => (dispatch) => {
  dispatch({
    type: 'USER_EDIT',
    payload: user,
  })
}
