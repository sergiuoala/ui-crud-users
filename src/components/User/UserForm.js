import React from 'react'
import { Form, Formik } from 'formik'
import { Button, MenuItem, TextField, Typography, Snackbar } from '@material-ui/core'
import get from 'lodash.get'

import UserSchema from './userSchema'
import AddressSchema from '../forms/addressSchema'
import { isValidField } from '../forms/form-utils'

export default function UserForm({
  initialState = {
    id: String(+new Date()),
    email: '',
    name: '',
    phone: '',
    gender: '',
    address: {
      city: '',
      zipcode: '',
      street: '',
      houseNumber: '',
    },
  },
  onFormSubmit,
}) {
  const validationSchema = UserSchema.concat(AddressSchema)
  const [showNotification, setShowNotification] = React.useState(false)

  return (
    <Formik
      initialValues={initialState}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          onFormSubmit(values)
          setSubmitting(false)
          setShowNotification(true)
        }, 400)
      }}
      validateOnChange={false}
      validateOnBlur={false}
    >
      {({ isSubmitting, values, handleChange, errors }) => (
        <Form>
          <TextField
            id="outlined-dense-multiline"
            label="Name"
            name="name"
            margin="normal"
            variant="outlined"
            fullWidth
            value={values.name}
            onChange={handleChange}
            helperText={errors.name ? errors.name : ''}
            error={isValidField(errors, 'name')}
          />
          <TextField
            id="outlined-dense-multiline"
            label="Email"
            name="email"
            margin="normal"
            variant="outlined"
            fullWidth
            value={values.email}
            onChange={handleChange}
            helperText={errors.email ? errors.email : ''}
            error={isValidField(errors, 'email')}
          />

          <TextField
            id="outlined-dense-multiline"
            label="Phone"
            name="phone"
            margin="normal"
            variant="outlined"
            fullWidth
            value={values.phone}
            onChange={handleChange}
            helperText={errors.phone ? errors.phone : ''}
            error={isValidField(errors, 'phone')}
          />

          <TextField
            id="outlined-select-currency"
            select
            label="Gender"
            value={values.gender}
            name="gender"
            onChange={handleChange}
            margin="normal"
            variant="outlined"
            fullWidth
            helperText={errors.gender ? errors.gender : ''}
            error={isValidField(errors, 'gender')}
          >
            <MenuItem value="male">Male</MenuItem>
            <MenuItem value="female">Female</MenuItem>
          </TextField>

          <Typography variant="h6">Address</Typography>

          <TextField
            id="outlined-dense-multiline"
            label="City"
            name="address.city"
            margin="normal"
            variant="outlined"
            fullWidth
            value={values.address.city}
            onChange={handleChange}
            helperText={get(errors, 'address.city', '')}
            error={get(errors, 'address.city', '').length > 0}
          />
          <TextField
            id="outlined-dense-multiline"
            label="Zipcode"
            name="address.zipcode"
            margin="normal"
            variant="outlined"
            fullWidth
            value={values.address.zipcode}
            onChange={handleChange}
            helperText={get(errors, 'address.zipcode')}
            error={isValidField(errors, 'address.zipcode')}
          />
          <TextField
            id="outlined-dense-multiline"
            label="Street"
            name="address.street"
            margin="normal"
            variant="outlined"
            fullWidth
            value={values.address.street}
            onChange={handleChange}
            helperText={get(errors, 'address.street')}
            error={isValidField(errors, 'address.street')}
          />
          <TextField
            id="outlined-dense-multiline"
            label="House Number"
            name="address.houseNumber"
            margin="normal"
            variant="outlined"
            fullWidth
            value={values.address.houseNumber}
            onChange={handleChange}
            helperText={get(errors, 'address.houseNumber')}
            error={isValidField(errors, 'address.houseNumber')}
          />
          <Button type="submit" variant="contained" color="primary" disabled={isSubmitting}>
            Save
          </Button>

          <Snackbar
            open={showNotification}
            onClose={() => setShowNotification(false)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            autoHideDuration={2000}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">User saved successfully!</span>}
          />
        </Form>
      )}
    </Formik>
  )
}
