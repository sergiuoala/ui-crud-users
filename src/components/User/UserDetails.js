import React from 'react'
import Title from '../Title'
import { deleteUserAction } from './userActions'

import { Button, ButtonGroup, Chip, Typography } from '@material-ui/core'
import { Edit, Delete, Email, Phone } from '@material-ui/icons'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

export function UserDetails({ user, onUserDelete }) {
  const { id, name, gender, email, phone, address } = user

  const handleDeleteUserClick = React.useCallback(() => {
    onUserDelete(id)
  }, [id, onUserDelete])

  return (
    <div>
      <Title>
        {name} <Chip label={gender} variant="outlined" />
      </Title>

      <Typography variant="h6">Contact details</Typography>

      <p>
        <Email /> <a href={`mailto:${email}`}>{email}</a>
      </p>
      <p>
        <Phone /> <a href={`tel:${phone}`}>{phone}</a>
      </p>

      <Typography variant="h6">Address</Typography>

      <p>
        {address.street} {address.houseNumber}, {address.city} {address.zipcode}
      </p>

      <ButtonGroup variant="contained" size="small" aria-label="small contained button group">
        <Button
          aria-label="edit"
          variant="contained"
          color="primary"
          to={`/users/edit/${id}`}
          component={Link}
        >
          <Edit />
          Edit User
        </Button>
        <Button
          aria-label="delete"
          variant="contained"
          color="secondary"
          onClick={handleDeleteUserClick}
        >
          <Delete />
          Delete user
        </Button>
      </ButtonGroup>
    </div>
  )
}

export function ConnectedUserDetails({ id, users, deleteUserAction }) {
  const user = users.find((u) => u && u.id === id)

  if (!user) {
    return <p>User with id {id} does not exist or has been deleted.</p>
  }

  return <UserDetails user={user} onUserDelete={deleteUserAction} />
}

const mapStateToProps = ({ users = [] }) => ({
  users,
})

const mapDispatchToProps = (dispatch) => ({
  deleteUserAction: (userId) => dispatch(deleteUserAction(userId)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedUserDetails)
