import users from './user-data'

export default (state = users, action) => {
  switch (action.type) {
    case 'USER_CREATE':
      return [...state, action.payload]
    case 'USER_DELETE':
      const { userId } = action.payload
      return state.filter(({ id }) => id !== userId)
    case 'USER_EDIT':
      const user = action.payload
      return state.map((u) => {
        if (u.id === user.id) {
          return user
        }
        return u
      })
    default:
      return state
  }
}
