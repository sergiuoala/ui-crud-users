/* eslint-disable no-script-url */

import React from 'react'
import { connect } from 'react-redux'
import Link from '@material-ui/core/Link'
import { Link as RouterLink } from 'react-router-dom'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Title from '../Title'

export function UsersList({ title = 'Users', users = [] }) {
  return (
    <div>
      <Title>{title}</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>#</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Phone</TableCell>
            <TableCell align="right">Gender</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((row, index) => (
            <TableRow key={row.id}>
              <TableCell>{index + 1}</TableCell>
              <TableCell>
                <Link to={`/users/${row.id}`} component={RouterLink}>
                  {row.name}
                </Link>
              </TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell>{row.phone}</TableCell>
              <TableCell align="right">{row.gender}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  )
}

const ConnectedUsersList = ({ users, recentLength, title }) => {
  const usersList =
    Number.isInteger(recentLength) && users.length > recentLength
      ? users.slice(users.length - recentLength).reverse()
      : users

  if (usersList.length === 0) {
    return (
      <div>
        <Title>{title}</Title>
        <p>No users. Go create one.</p>
      </div>
    )
  }

  return <UsersList users={usersList} title={title} />
}

const mapStateToProps = ({ users = [] }) => ({ users })

export default connect(mapStateToProps)(ConnectedUsersList)
