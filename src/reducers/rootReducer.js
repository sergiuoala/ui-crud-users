import { combineReducers } from 'redux'
import users from '../components/User/usersReducer'

export default combineReducers({
  users,
})
